'''
Created on May 1,2017

@author: Ashmita Gopal
'''
import zipfile
from cStringIO import StringIO
import os
import sys
from threading import Thread
import base64
from lxml import etree,html
from PIL import Image
from ebooklib import epub
import urllib2
from car.misc import program_path
from car.document import Document
from car.document.docx.paragraph import parseParagraph, parseTable, addToBody, IMAGE_TRANSLATION
from BeautifulSoup import BeautifulSoup as Soup,Tag, NavigableString
import cgi

ROOT_PATH = program_path('car/epub')

w_NS = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'
r_NS = '{http://schemas.openxmlformats.org/package/2006/relationships}'
c_NS = '{http://purl.org/dc/elements/1.1/}'


class EPUB(Document):
    '''
    Imports a .epub and transforms it to meet my needs.

    The progressCallback expects a function that will handle the following
    arguments:

    progressCallback(percentageOutOf100)

    The checkCancelFunction is a function that returns True when we should stop
    the import process and False otherwise.
    '''

    def __init__(self, epubFilePath, progressHook, cancelHook):
        '''
        Generates the document structure from the .epub file.
        '''
        Document.__init__(self, epubFilePath, progressHook, cancelHook)

        self._name = os.path.splitext(os.path.basename(epubFilePath))[0]

        self._progressHook(0, 'Starting up...')

        book = epub.read_epub(epubFilePath)

        #print book
        # append html pages for final html page
        html_final_con = ''
        book_html_list=[]
        for items in enumerate(book.items):
            if items[1].media_type == 'application/xhtml+xml' and items[1].id != 'cover':
                book_html_list.append((items[1].content))

        #print book_html_list

        for html_page in book_html_list:
            html_value = parse_html_string(html_page)
            root = html_value.getroottree()
            body_list = root.findall('body')
            for body in body_list:
                string_start = html.tostring(body)
                for elem in body.getchildren():
                    string_bc = html.tostring(elem)
                    html_final_con = html_final_con+ '\n' + string_bc

        #print html_final_con
        import lxml
        html_chk = parse_html_string("<html><body></body></html>")
        root = html_chk.getroottree()
        html_final_body = root.find('body')
        html_final_body.insert(0,lxml.html.fromstring(html_final_con))

        #Insert heading for title
        string_h_title = '<title>' + book.title + '</title>'
        html_final =lxml.html.fromstring(string_h_title)
        root_h_final = html_final.getroottree()
        html_mark = root_h_final.find('head')
        html_mark = html_mark.getparent()
        html_mark.append(html_final_body)
        #print html_final

        progress = 25
        self._progressHook(progress, 'Reading in paragaphs...')

        # Save images to temp folder
        #image_list = book.get_items_of_type(epub.EpubImage)
        book_image_list = []
        for items in enumerate(book.items):
             if "image" in items[1].media_type and items[1].id != 'cover':
                 book_image_list.append((items[1]))
        #print book_image_list

        #Make my images directory
        if not os.path.isdir(self._tempFolder + '/images'):
            os.makedirs(self._tempFolder + '/images')

        # Open a zip file of my docx file
        z = zipfile.ZipFile(epubFilePath, 'r')
        i = 0
        for f in z.namelist():
            i += 1
            #.jpg', '.jpeg', '.gif', '.tiff', '.tif', '.png'
            if (f.find('jpg') >= 0 or f.find('jpeg') >= 0 or f.find('gif') >= 0 or f.find('tiff') >=0 or f.find('tif') >= 0 or f.find('png') >=0) :
                if os.path.splitext(f)[1].lower() != '.wmf':
                    # Extract it to my import folder
                    currPath = os.path.basename(f)
                    savePath = self._tempFolder + '/images/' + currPath

                    # Only do the image translation on Windows. PIL doesn't like to
                    # be frozen on Macs
                    # TODO: Make PIL work when frozen on Macs
                    if (os.path.splitext(savePath)[1].lower() in IMAGE_TRANSLATION) and (sys.platform == 'win32'):
                        try:
                            contents = z.read(f)
                            myFile = StringIO(contents)
                            convertFile = Image.open(myFile)
                            outPath = os.path.splitext(savePath)[0] + IMAGE_TRANSLATION[
                                os.path.splitext(savePath)[1].lower()]
                            convertFile.save(outPath)
                        except IOError as e:
                            # Don't try to do anything else with it. Just copy the
                            # file over
                            print 'Could not convert image', f, 'to', os.path.basename(savePath)
                            with open(savePath, 'wb') as imageFile:
                                imageFile.write(z.read(f))
                    else:
                        with open(savePath, 'wb') as imageFile:
                            imageFile.write(z.read(f))



        z.close()
        images = html_final.xpath("//img")
        #print images
        progress = 50
        self._progressHook(progress, 'Reading images...')

        import urlparse
        for i in range(len(images)):
            try:
                p = os.path.basename(images[i].get('src'))
                filepath =os.path.join((self._tempFolder),'images',p)
                filepath = filepath.replace('\\','/')
                images[i].set('src',filepath.decode('string_escape'))
                if images[i].get('alt') is not None:
                    images[i].set('title',images[i].get('alt'))
            except urllib2.URLError as e:
                # The URL may already be embedded, so don't do it twice.
                 pass



        #print html.tostring(html_final)
        m_NS = 'http://www.w3.org/1998/Math/MathML'
        mathList = {}
        mathList = html_final.xpath('.//math | .//m:math', namespaces={'m' : m_NS})
        for math_c in mathList:
            root = html_final.find('body')
            parent_math = math_c.getparent()
            html_append =  etree.SubElement(parent_math,"span")
            html_append.set('class', 'mathmlEquation')
            html_append.insert(0,math_c)
            mathml = html.tostring(html_append)
            amhtml = html.fromstring(mathml)
            math_c.getparent().replace(math_c, amhtml)

        #print html.tostring(html_final)
        self._contentDOM = html_final
        progress = 75
        self._progressHook(progress, 'Reading in math...')
        self._mapMathEquations()


def parse_html_string(s):
    from lxml import html

    utf8_parser = html.HTMLParser(encoding='utf-8')

    html_tree = html.document_fromstring(s, parser=utf8_parser)

    return html_tree

def add_title_code(webpageString, titleString):
    import lxml
    root = webpageString
   #string_h_title= '<head><title>'+ titleString +'</title></head>'
    #head_tit_add = lxml.html.fromstring(string_h_title)
    head = root.find('head')
    if head == None:
        where = 0
        head_to_add = etree.Element(root,"head")
        title_add = etree.SubElement(head_to_add, "title")
        title_add.text = titleString
        root.insert(where,head_to_add)
    else:
        title = head.find('title')
        title_add = etree.SubElement(head,"title")
        title_add.text = titleString
        if title == None:
            where = 0
        head.insert(where, title_add)
    return lxml.html.tostring(root)

if __name__ == '__main__':
    pass
