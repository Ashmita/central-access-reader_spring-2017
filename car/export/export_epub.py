
# -*- coding: UTF-8 -*-
from car.export import ExportThread
from PyQt4.QtGui import QMessageBox
from lxml import etree
from lxml import html
from car.gui import configuration
from io import BytesIO
import uuid
import os
import zipfile
import urllib
import os.path
import mimetypes
import time
import urlparse
import cgi
from BeautifulSoup import BeautifulSoup as Soup,Tag, NavigableString
from readability.readability import Document
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
import textwrap
import string


w_NS = '{http://schemas.openxmlformats.org/wordprocessingml/2006/main}'

class MyZipFile(zipfile.ZipFile):
     def writestr(self, name, s, compress=zipfile.ZIP_DEFLATED):
         zipinfo = zipfile.ZipInfo(name, time.localtime(time.time())[:6])
         zipinfo.compress_type = compress
         zipfile.ZipFile.writestr(self, zipinfo, s)

class EpubExportThread(ExportThread):
    '''
       Exports the content to a EPUB doc with embedded images and math
       descriptions.
    '''
 # Arbitrary portions of the progress bar that the different stages take up
    PERCENT_CONVERTING_MATH = (0.0, 75.0)
    PERCENT_EMBED_IMAGES = (PERCENT_CONVERTING_MATH[1], 100.0)

    # Set the label for each of the states
    DESCRIPTION_CONVERTING_MATH = 'Converting equation '
    DESCRIPTION_EMBED_IMAGES = 'Embedding image '


    def __init__(self, document, htmlContent, tempDirectory):
        super(EpubExportThread, self).__init__(document, htmlContent, tempDirectory)
        self._toc = None
        self._title = None
        self._creator = None
        self._language = None
        self._lastProgress = -1

    @staticmethod
    def description():
        return '[X-HTML] EPub '

    @staticmethod
    def getDefaultPath(inputFilePath):
        '''
        Returns a possible default path for the given input file.
        '''
        return os.path.splitext(inputFilePath)[0] + '.epub'



    def run(self):
        super(EpubExportThread, self).run()

        mainHtml = html.fromstring(self._document.getMainPage(mathOutput='svg'))

        '''toc = ''
        if configuration.getBool('AddTOC', True):
            self._toc = self._createTableOfContents(mainHtml)
        '''
        fileName = str(self._document.getFilePath())
        fileType = string.split(fileName, '.')[1]
        #print fileType
        if fileType == 'epub':
            self.isSuccess = False
            print 'Could not export file', self._document.getFilePath, ':', "CAR does not support EPUB - EPUB Conversion."
            QMessageBox.about(self, "File Export Error", "CAR does not support EPUB - EPUB Conversion.")
            self.error.emit("CAR does not support EPUB - EPUB Conversion.")
            self._success = False

        if fileType != 'epub':
            self._title = self._document.docTitle
            self._creator = self._document.author
        #self._language = self._document.language

        ########outputFileName = str(fileDir)+'.epub'

        #print outputFileName
        #print self._filePath

        myEPUB = MyZipFile(self._filePath,'w',zipfile.ZIP_DEFLATED)
        # mimetype
        myEPUB.writestr("mimetype","application/epub+zip",zipfile.ZIP_STORED)
        #Ref for index file
        myEPUB.writestr("META-INF/container.xml",'''<?xml version="1.0" encoding="utf-8" standalone="no"?>
                                                    <container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">
                                                    <rootfiles><rootfile full-path="OEBPS/Content.opf" media-type="application/oebps-package+xml"/></rootfiles>
                                                    </container>''')
        #index file
        index_template = '''<?xml version='1.0' encoding="utf-8"?><package version="2.0" xmlns="http://www.idpf.org/2007/opf" unique-identifier="bookid">
                        <metadata xmlns:dc="http://purl.org/dc/elements/1.1/">
                        <dc:identifier  id="bookid">%(ISBN)s</dc:identifier>
                        <dc:title >%(title)s</dc:title>
                        <dc:rights>Copyright 2017</dc:rights>
                        <dc:publisher></dc:publisher>
                        <dc:subject></dc:subject>
                        <dc:date>%(date)s</dc:date>
                        <dc:description></dc:description>
                        <dc:creator>%(creator)s</dc:creator>
                        <dc:language>%(language)s</dc:language>
                        <meta name="cover" content="cover-image"/>
                      </metadata>
                      <manifest>
                        <item id="ncxtoc" media-type="application/x-dtbncx+xml" href="toc.ncx"/>
                        <item id="cover" href="cover.html" media-type="application/xhtml+xml"/>
                        <item id="cover-image" href="images/cover.jpg" media-type="image/jpeg"/>
                        %(manifest)s
                      </manifest>
                      <spine toc="ncxtoc">
                        <itemref idref="cover" linear="no"/>
                        %(spine)s
                      </spine>
                      <guide>
                        <reference href="cover.html" type="cover" title="Cover" />
                      </guide>
                      </package>'''

        #table of contents
        toc_template = '''<?xml version="1.0" encoding="utf-8" standalone="no"?>                           
                          <ncx xmlns="http://www.daisy.org/z3986/2005/ncx/" version="2005-1">
                          <head>
                            <meta name="dtb:uid" content="%(ISBN)s"/>
                            <meta name="dtb:depth" content="-1"/>
                            <meta name="dtb:totalPageCount" content="0"/>
                            <meta name="dtb:maxPageNumber" content="0"/>
                          </head>
                          <docTitle>
                            <text>%(title)s</text>
                          </docTitle>
                          <navMap>
                            <navPoint id="navpoint-1" playOrder="1">
                                <navLabel>
                                    <text>Cover</text>
                                </navLabel>
                                    <content src="cover.html"/>
                            </navPoint>
                            %(toc)s
                          </navMap>          
                          </ncx>'''

        #cover template
        cover_template = '''<?xml version="1.0" encoding="utf-8" standalone="no"?>
                            <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
                            <html xmlns="http://www.w3.org/1999/xhtml">
                              <head>
                                <title>Cover</title>
                                <style type="text/css"> img { max-width: 100%%; }</style>
                              </head>
                              <body>
                              <h1>%(title)s</h1>
                                <div id="cover-image">
                                  <img src="%(front_cover)s" alt="Front Cover Image"/>
                                </div>
                              </body>
                            </html>'''

        #no style sheet template
        #default cover image
        canvas_x = 600
        canvas_y = 800
        img_new =Image.new('RGB', (canvas_x, canvas_y))


        draw = ImageDraw.Draw(img_new)
        # font = ImageFont.truetype(<font-file>, <font-size>)
        font = ImageFont.load_default()
        font_title = ImageFont.load_default()
        titleFontSize=35
        try:
            font_title = ImageFont.truetype("car/font/arialbd.ttf", titleFontSize)
            font = ImageFont.truetype("car/font/arialbd.ttf",20)
            #print font.getname()
        except:
            #print "Unexpected error:", sys.exc_info()
            pass
        # draw.text((x, y),"Sample Text",(r,g,b))
        #draw.text((250, 350), str(self._title), (255, 255, 255), font=font)
        #draw.text((250, 450), str(self._creator), (255, 255, 255), font=font)
        #draw.text((canvas_x * 0.5 - draw.textsize(str(self._title))[0] * 0.5, canvas_y * 0.03),fill=(255, 255, 255), text=str(self._title), font=font)
        margin = 30
        offset = 50
        for line in textwrap.wrap(str(self._title), width=30):
            draw.text((margin, offset), line, font=font_title, fill=(255, 255, 255))
            offset += (font.getsize(line)[1]+10)

        margin_A = 30
        offset_A =offset +30
        split_author =''
        if str(self._creator).find(';')!=-1:
            split_author= str(self._creator).split(";")
        else:
            split_author = str(self._creator)

        check_string ='string'
        if (split_author.__class__() == check_string.__class__()):
            for line in textwrap.wrap(split_author, width=30):
                draw.text((margin_A, offset_A), line, font=font, fill=(255, 255, 255))
                offset_A += (font.getsize(line)[1] + 10)
        else:
            for author in split_author:
                for line in textwrap.wrap(author, width=30):
                    draw.text((margin_A, offset_A), line, font=font, fill=(255, 255, 255))
                    offset_A += (font.getsize(line)[1]+10)
        image_file = BytesIO()
        img_new.save(image_file, 'JPEG')
        ##img.save(os.path.join(os.path.normpath(fileDir), 'OEBPS/images/cover.jpg'))
        myEPUB.writestr('OEBPS/images/' + 'cover.jpg', image_file.getvalue())
        cpath = 'images/cover.jpg'
        id= uuid.uuid1()
        fake_isbn = id.time

        info = dict(
            title=self._title,
            creator=self._creator,
            language=self._language,
            date=time.strftime('%Y-%m-%d'),
            front_cover=cpath,
            front_cover_type='image/jpeg',
            ISBN = fake_isbn
        )
        myEPUB.writestr('OEBPS/cover.html',cover_template % info)

        

        # Regenerate the content to use SVGs. They render better and more
        # consistently.

        self._htmlContent = mainHtml
        #print mainHtml
        if self._running:
            self._convertPageNumbersToH6(self._htmlContent)
            self._removeAltTextIfIgnored(self._htmlContent)
            self._removeHighlighter(self._htmlContent)
            self._removeScripts(self._htmlContent)
            self._removeLinkToCSS(self._htmlContent)
            # detect and remove all mathml from the html doc
            #create a data dictionary of math equation and unique identifiers to identify placeholders
            math_list = self._htmlContent.xpath("//span[@class='mathmlEquation']")
            eq_dict = {}
            if(math_list is not None):
                if math_list.__len__() > 0 :
                    #print math_list
                    for i in range(len(math_list)):
                        content = ''
                        content = etree.tostring(math_list[i])
                        eqn_no = 'equation' + str(i);
                        eq_dict[eqn_no]= content
                        replace_string ='<span>%('+eqn_no+')s</span>'
                        element_replace = etree.fromstring(replace_string)
                        parent_math = math_list[i].getparent()
                        parent_math.replace(math_list[i],element_replace)
                        #print html.tostring(self._htmlContent)
            self._addFrontMatterH1(self._htmlContent)
            self._addEndofDocumentH1(self._htmlContent)
            ##headings_HTML = singleHTML.xpath('//h1')
            singleHTML = self._htmlContent
            #print html.tostring(singleHTML)

            singleHTML_toString = html.tostring(singleHTML)

            soup = Soup(singleHTML_toString)
            pages = []
            # headings_HTML = soup.findAll('h1')
            #
            # for h1tag in headings_HTML:
            #     page = [str(h1tag)]
            #     elem = h1tag.nextSibling
            #     while elem and elem.name != 'h1':
            #         page.append(str(elem))
            #         elem = elem.nextSibling
            #     pages.append('\n'.join(page))

            headingh1_HTML =soup.findAll('h1')
            if headingh1_HTML is not None:
                hh1_length = headingh1_HTML.__len__()
                if hh1_length >= 3:
                    for h1tag in headingh1_HTML:
                        page = [str(h1tag)]
                        elem = h1tag.nextSibling
                        if elem != '\n':
                            while elem and elem.name != 'h1':
                                page.append(str(elem))
                                elem=elem.nextSibling
                            pages.append('\n'.join(page))

                    page_toParse = str(pages[1]).replace('>\n<', '><')
                    soup2 = Soup(page_toParse)
                    headingsh2_HTML = soup2.findAll(['h1', 'h2'])
                    del pages[-1]
                    #del pages[-1]
                    # insert_h2_tag = Tag(soup2, 'h2')
                    # heading_to_append = NavigableString('END OF PAGE')
                    # heading_to_append.name = 'string'
                    # insert_h2_tag.insert(0, heading_to_append)
                    # insert_h2_tag.id = 'heading2_EOF'
                    # insert_h2_tag.name = 'h2'
                    # # bodytag.insert_after(insert_h1_tag)
                    # soup2.insert(len(soup2.contents), insert_h2_tag)
                    # # soup.body.replaceWith(body)
                    # soup2.prettify()

                    for h2tag in headingsh2_HTML:
                        page = [str(h2tag)]
                        elem = h2tag.nextSibling
                        while (elem and elem.name != 'h2'):
                            page.append(str(elem))
                            elem = elem.nextSibling
                        pages.append('\n'.join(page))

                else:
                    count_element = 0
                    for h1tag in headingh1_HTML:
                        count_element = 0
                        page = [str(h1tag)]
                        elem = h1tag.nextSibling
                        while elem and elem.name != 'h1':
                            page.append(str(elem))
                            elem = elem.nextSibling
                            count_element = count_element+1
                        if (count_element >= 1):
                            pages.append('\n'.join(page))

                    page_toParse = str(pages[0]).replace('>\n<','><')
                    soup2 = Soup(page_toParse)
                    headingsh2_HTML = soup2.findAll(['h1', 'h2'])
                    del pages[-1]


                    # insert_h2_tag = Tag(soup2, 'h2')
                    # heading_to_append = NavigableString('END OF PAGE')
                    # heading_to_append.name = 'string'
                    # insert_h2_tag.insert(0, heading_to_append)
                    # insert_h2_tag.id = 'heading2_EOF'
                    # insert_h2_tag.name = 'h2'
                    # # bodytag.insert_after(insert_h1_tag)
                    # soup2.insert(len(soup2.contents), insert_h2_tag)
                    # # soup.body.replaceWith(body)
                    soup2.prettify()


                    for h2tag in headingsh2_HTML:
                        page = [str(h2tag)]
                        elem = h2tag.nextSibling
                        #print elem
                        while (elem and elem.name != 'h2'):
                            page.append(str(elem))
                            elem = elem.nextSibling
                        pages.append('\n'.join(page))

            #singleHTMLTree = etree.parse(singleHTML)
            #headings = singleHTMLTree.findall('./{0}body/{0}sdt/{0}sdtPr/{0}rPr/{0}lang'.format(w_NS))

            #print pages
            #remove the last element of pages
            #del pages[-1]

            manifest = ""
            spine = ""
            toc = ""
            for i,p in enumerate(pages):
                #print "Inside pages loop"
                if(p != '<h1>Contents</h1>'):
                    html_doc_new = """
                                    <html>
                                    </html
                                    """
                    read_article = Document(p).summary().encode('utf-8')
                    soup = ""
                    soup_toinsert = Soup(read_article)
                    if(soup_toinsert.html is not None):
                        soup = soup_toinsert
                    else:
                        soup = Soup(html_doc_new)
                        html_tag = soup.html
                        html_tag.insert(0,soup_toinsert)
                    soup.html["xmlns"] = "http://www.w3.org/1999/xhtml"
                    if soup.find('h1') is not None:
                        read_title = soup.find('h1').text

                    else:
                        if soup.find('h2') is not None:
                            read_title = soup.find('h2').text
                        else:
                            read_title = "Content_"+ str(i+1)

                    manifest += '<item id="page_%s" href="page_%s.html" media-type="application/xhtml+xml" />\n' %(i+1,i+1)
                    spine += '<itemref idref="page_%s"/>\n' %(i+1)
                    toc += '<navPoint id="navpoint-%s" playOrder="%s"><navLabel><text>%s</text></navLabel><content src="page_%s.html"/></navPoint>' % (i+2,i+2,cgi.escape(read_title),i+1)


                    if read_title is not None:
                        header = soup.html
                        head=Tag(soup,"head")
                        insert_h1_tag = Tag(soup, 'h1')
                        heading_to_append = cgi.escape(read_title)
                        insert_h1_tag.insert(0, heading_to_append)
                        insert_h1_tag.id = 'heading_FrontMatter'
                        head.insert(0,insert_h1_tag)
                        header.insert(0,head)



                    for j,image_tag in enumerate(soup.findAll("img")):
                        fullpath_img =urlparse.urljoin(p,image_tag["src"])
                        imgpath = urlparse.urlunsplit(urlparse.urlsplit(fullpath_img)[:3]+('','',))
                        imgfile = os.path.basename(imgpath)
                        filename = 'path_%s_image_%s%s' % (i+1,j+1,os.path.splitext(imgfile)[1])
                        if imgpath.lower().startswith("http"):
                            myEPUB.writestr('OEBPS/images/'+filename,urllib.urlopen(imgpath).read())
                            image_tag['src'] = 'images/' + filename
                            manifest +='<item id="page_%s_image_%s" href="images/%s" media-type="%s"/>\n' % (i+1,j+1,filename,mimetypes.guess_type(filename)[0])
                        if imgpath.lower().startswith("file"):
                            myEPUB.writestr('OEBPS/images/' + filename, urllib.urlopen(imgpath).read())
                            image_tag['src'] = 'images/' + filename
                            manifest += '<item id="page_%s_image_%s" href="images/%s" media-type="%s"/>\n' % (
                            i + 1, j + 1, filename, mimetypes.guess_type(filename)[0])

                    for k, table_tag in enumerate(soup.findAll("table")):
                        table_tag['border'] = '1'

                    string_page = str(soup)
                    #Replacing math equations into placeholders to solve issue of random rearrangement by BS4  parser
                    string_page = string_page % eq_dict
                   # string_page.replace("<table>","<table border='1'>")
                    myEPUB.writestr('OEBPS/page_%s.html' % (i+1),string_page)

                info['manifest'] = manifest.decode('utf-8')
                info['spine'] = spine.decode('utf-8')
                info['toc'] = toc.encode('ascii', 'ignore').decode('ascii')


            if self._running:


                #with open(self._filePath, 'wb') as f:
                #f.write(html.tostring(self._htmlContent))

                '''epub = zipfile.ZipFile("%s.zip" % (absoluteFilePath), "w", zipfile.ZIP_DEFLATED)'''
                myEPUB.writestr('OEBPS/Content.opf',index_template % info)
                myEPUB.writestr('OEBPS/toc.ncx',toc_template % info)
                # Say that this export finished successfully
                self.success.emit()
                self.isSuccess = True


    def _myOnProgress(self, percent, label):
        self._reportProgress(percent, label)

    def _reportProgress(self, percent, label, alwaysUpdate=False):
        myPercent = int(percent)
        if (myPercent != self._lastProgress) or alwaysUpdate:
            self._lastProgress = myPercent
            self.progress.emit(myPercent, label)

    def _createTableOfContents(self, myHtml):
        '''
        Create a table of contents.
        '''
        # Generate a table of contents from the headings
        headingMap = {'h1': 1,
                      'h2': 2,
                      'h3': 3,
                      'h4': 4,
                      'h5': 5}
        headings = []
        leastHeadingLevel = 500000
        for ev, elem in etree.iterwalk(myHtml, events=('end',)):
            if elem.tag in headingMap:
                headings.append((headingMap[elem.tag], self._getTextInsideElement(elem), elem))
                if headingMap[elem.tag] < leastHeadingLevel:
                    leastHeadingLevel = headingMap[elem.tag]

        navRoot = html.Element('nav')
        navRoot.set('role', 'navigation')
        navRoot.set('class', 'table-of-contents')

        # Check if need warning about page number conversion
        elem1 = html.Element('p')
        elem1.text = 'Screen Reader Users: HTML Flex has been optimized for IE + JAWS 16 and iOS and Mac + VoiceOver. ' \
                        'An internet connection is required.'
        navRoot.append(elem1)

        query = myHtml.find(".//p[@class='pageNumber']")
        if query is not None:
            elem2 = html.Element('p')
            elem2.text = 'Page numbers have been converted to heading 6.'
            navRoot.append(elem2)

        elem = html.Element('h1')
        elem.text = 'Contents:'
        navRoot.append(elem)

        elem = html.Element('ul')
        navRoot.append(elem)

        currentLevel = 1
        headingId = 0
        parent = elem
        for h in headings:

            # Indent/deindent to right level
            while h[0] != currentLevel:
                if currentLevel < h[0]:
                    elem = html.Element('ul')
                    parent.append(elem)
                    parent = elem
                    currentLevel += 1

                if currentLevel > h[0]:
                    parent = parent.getparent()
                    currentLevel -= 1


            # Make element for the thing
            elem = html.Element('li')
            link = html.Element('a')
            link.set('href', '#heading' + str(headingId))
            elem.append(link)
            link.text = h[1]

            # Set the link
            link.set('href', '#heading' + str(headingId))
            h[2].set('id', 'heading' + str(headingId))
            headingId += 1

            parent.append(elem)

        #Return Toc
        return navRoot

    def _convertPageNumbersToH6(self, myHtml):
        '''
        Converts all of the page numbers into H6 headings so that they are
        easily navigable by JAWS and other screen-reading software.
        '''
        # Get all of the page numbers
        pageNumbers = myHtml.xpath("//p[@class='pageNumber']")

        # Convert each page number to an h6 element
        for p in pageNumbers:
            if not self._running:
                break
            p.tag = 'h6'
            p.attrib.pop('class')

    def _addEndofDocumentH1(self, myHtml):
        '''
            Add a new H1 heading "END OF DOCUMENT" to end of the document
        '''
        def first_child_element(elem):
            while elem is not None:
                # Find next element, skip NavigableString objects
                elem = elem.contents[0]
                return elem

        html_new = html.tostring(myHtml)
        #print html_new

        soup = Soup(html_new)
        bodytag = soup.find('/body')
        elem = first_child_element(bodytag)
        insert_h1_tag = Tag(soup,'h1')
        heading_to_append = NavigableString('END OF DOCUMENT')
        insert_h1_tag.insert(0,heading_to_append)
        insert_h1_tag.id='heading_EOF'
        insert_h1_tag.name = 'h1'
        #bodytag.insert_after(insert_h1_tag)
        soup.body.insert(len(soup.body.contents),insert_h1_tag)
        #soup.body.replaceWith(body)
        soup.body.prettify()
        self._htmlContent = html.fromstring(str(soup.renderContents()))
       # firsth1tag = soup.find('h1')

        #page = ''
        #count = 0
        #elem = firsth1tag.nextSibling
        #while elem and elem.name != 'h1':
         #    page.append(str(elem))
          #   elem = elem.nextSibling
           #  count=count+1
        #if (count  == 0):
         #   self._htmlContent =myHtml
            ##return myHtml
        #else:
          #  self._htmlContent = html.fromstring(str(soup.renderContents()))
            ##return str(soup.renderContents())

    def _addFrontMatterH1(self, myHtml):
        '''
            Add a new H1 heading "FRONT MATTER" to start of the document
        '''
        def first_child_element(elem):
            while elem is not None:
                # Find next element, skip NavigableString objects
                elem = elem.contents[0]
                return elem

        html_new = html.tostring(myHtml)
        #print html_new

        soup = Soup(html_new)
        #etree_parse = etree.fromstring(html_new)
        bodytag = soup.find('body')
        elem = first_child_element(bodytag)
        insert_h1_tag = Tag(soup,'h1')
        heading_to_append = NavigableString('FRONT MATTER')
        heading_to_append.name = 'string'
        insert_h1_tag.insert(0,heading_to_append)
        insert_h1_tag.id='heading_FrontMatter'
        insert_h1_tag.name = 'h1'
        #bodytag.insert_after(insert_h1_tag)
        soup.body.insert(0,insert_h1_tag)
        #soup.body.replaceWith(body)
        soup.body.prettify()
        h1tags = soup.findAll('h1')
        if h1tags is not None:
            length = h1tags.__len__()
            if length == 1:
                soup = Soup(html_new)
                html_soup =soup.html
                bodytag = soup.find('body')
               # html_soup["xmlns"] = "http://www.w3.org/1999/xhtml"
               # html_soup["xmlns:mathml"] = "http://www.w3.org/1998/Math/MathML"
                elem = first_child_element(bodytag)
                insert_h1_tag = Tag(soup, 'h1')
                heading_to_append = NavigableString('Contents')
                heading_to_append.name = 'string'
                insert_h1_tag.insert(0, heading_to_append)
                insert_h1_tag.id = 'heading_Contents'
                insert_h1_tag.name = 'h1'
                # bodytag.insert_after(insert_h1_tag)
                soup.body.insert(0, insert_h1_tag)
                self._htmlContent = html.fromstring(str(soup))

            else:
                firsth1tag = soup.find('h1')

                page = [str('')]
                count = 0
                elem = firsth1tag.nextSibling
                while elem and elem.name != 'h1':
                    page.append(str(elem))
                    elem = elem.nextSibling
                    count = count + 1
                if (count == 0):
                    self._htmlContent = myHtml
                    ##return myHtml
                else:
                    self._htmlContent = html.fromstring(str(soup))
                    ##return str(soup.renderContents())








    def _removeAltTextIfIgnored(self, myHtml):
        '''
        Removes the alt text from images if the settings say to remove it.
        '''
        if configuration.getBool('IgnoreAltText', False):
            imgs = myHtml.xpath('//img')
            for i in imgs:
                i.attrib.pop('alt', None)
                i.attrib.pop('title', None)

    def _removeHighlighter(self, myHtml):
        '''
        Removes the highlighter from the document, if any.
        '''
        highlights = myHtml.xpath("//span[@id='npaHighlight']")
        highlightLines = myHtml.xpath("//span[@id='npaHighlightLine']")
        highlightSelections = myHtml.xpath("//span[@id='npaHighlightSelection']")

        for h in highlights:
            if not self._running:
                break
            self._replaceWithChildren(h)

        for h in highlightLines:
            if not self._running:
                break
            self._replaceWithChildren(h)

        for h in highlightSelections:
            if not self._running:
                break
            self._replaceWithChildren(h)

    def _replaceWithChildren(self, elem):
        '''
        Replaces the element with its own children.
        '''
        p = elem.getparent()
        i = p.index(elem)

        # Mess with the text
        if elem.text is not None:
            firstHalf = p[:i]
            if len(firstHalf) > 0:
                if firstHalf[-1].tail is not None:
                    firstHalf[-1].tail += elem.text
                else:
                    firstHalf[-1].tail = elem.text
            else:
                if p.text is not None:
                    p.text += elem.text
                else:
                    p.text = elem.text

        # Mess with the tail
        if elem.tail is not None:
            if len(elem) > 0:
                if elem[-1].tail is not None:
                    elem[-1].tail += elem.tail
                else:
                    elem[-1].tail = elem.tail
            else:
                if elem.text is not None:
                    p.text += elem.tail
                else:
                    p.text = elem.tail

        # Get all of the children of element and insert
        # them in the parent, in correct order
        childs = []
        for c in elem:
            childs.append(c)
        childs.reverse()
        for c in childs:
            p.insert(i, c)

        # Remove the element
        p.remove(elem)

    def _removeScripts(self, myHtml):
        '''
        Removes all the <script> elements from the document. No need for
        showing everyone the JavaScript.
        '''
        for e in myHtml.xpath('.//script'):
            e.getparent().remove(e)

    def _removeLinkToCSS(self, myHtml):
        '''
        Removes all the <script> elements from the document. No need for
        showing everyone the JavaScript.
        '''
        for e in myHtml.xpath('.//link'):
            e.getparent().remove(e)


    def _getTextInsideElement(self, elem):
        myText = ''
        if elem.text is not None:
            myText += elem.text

        for c in elem:
            myText += self._getTextInsideElement(c)
            if c.tail is not None:
                myText += c.tail

        return myText